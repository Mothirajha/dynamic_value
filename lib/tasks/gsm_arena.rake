namespace :gsmarena do
    desc "Get Url of all mobiles with respective to brands"
    task mobile_url: :environment do
        brands = {1 => "nokia"} #,9 => "samsung",4 => "motorola",7 => "sony",20 => "lg",48 => "apple",45 => "htc",36 => "blackberry", 41 => "hp", 58 => "huawei", 59 => "acer", 46 => "asus", 5 => "alcatel", 53 => "vodafone", 55 => "t-mobile", 44 => "toshiba", 47 => "gigabyte", 32 => "pantech", 62 => "zte", 85 => "xolo", 66 => "micromax", 67 => "blu", 68 => "spice", 83 => "karbonn", 86 => "prestigio", 70 => "verykool", 91 => "unnecto", 87 => "Maxwest", 75 => "celkon", 92 => "gionee" , 79 => "niu", 78 => "yezz", 81 => "parla", 72 => "plum" }

        def gsm(brands)
            brands.keys.each do |i|    
                domain="gsmarena.com"
                first = "http://www.gsmarena.com/celkon-phones-f-#{i}-2-p"
                pagin = "div.nav-pages > a"
                pagin_s = 1
                appen =""
                appen2=".php"
                anch = "div.makers // a"
                get_url(domain,first,pagin,appen,nil,nil,pagin_s,anch,nil,nil,1,appen2,brands[i])
            end 
        end

        def page_break(array)
            arr = []
            n = array[0]
            b = array[1]
            for i  in 0..n
              if (i % b == 0)
               arr.push(i)
              end
            end
            arr
        end

        def j_url(domain,url)
            if url[0] == "/"
              url.slice!(0)    
            end
           "http://www." + domain + "/" + url
        end

        def anchor(page,anch,url_join,domain,title_flag)
          page.search(anch).each do |j|
            if title_flag.kind_of?(String)
                n = [title_flag,j.text].join(" ")    
            elsif title_flag == 0
                n = j.text
            elsif title_flag == 1
                n = j.attr("title")
            end    
            u = j.attr("href")
            if url_join == 1
                u = j_url(domain,u)
            end
            if n.nil?
                n = j.text
            end
            # @out = "|"+[n,u].join("|") + "\n"
            # p @out
            GsmArena.perform_in(1.minute, u)
            #File.open(domain, 'a+') {|f| f.write(@out) }
            #GsmUrl.create(:name => n, :url => u)
          end  
        end


        def get_url(domain,first,pagin,appen,name = nil,url = nil,pagin_s = 1,anch = 0,sub = nil,sub2 = nil,url_join = 0,appen2 = nil,title_flag = 1)
            agent = Mechanize.new
            if pagin.kind_of?(String)
                page = agent.get(first + appen.to_s + pagin_s.to_s + appen2.to_s)
                pagin_e =  page.search(pagin).blank? ? 1 : page.search(pagin)[-2].text
                pagination = "#{pagin_s}..#{pagin_e}"
            elsif pagin.kind_of?(Integer)
                pagination = "#{pagin_s}..#{pagin}"
            elsif pagin.kind_of?(Array)
                pagination = page_break(pagin).to_s
            elsif pagin.nil?
                pagination = "[nil]"
            end
            p "first .. #{first}"
            p "appen .. #{appen}"
            p "pagin_s .. #{pagin_s}"
            p "pagination .. #{pagination}"
            p "appen2 .. #{appen2}"
            p "domain .. #{domain}"

            for i in eval(pagination)
                yourl = first + appen.to_s + i.to_s + appen2.to_s
                page = agent.get(yourl)
                p "Url is #{yourl}.."
                p "Processing for page no #{i}.."

                if anch != 0
                    p "Triggered anchor not equal to zero.."
                    anchor(page,anch,url_join,domain,title_flag)
                end

            end

        end  
        gsm(brands)      
    end
end