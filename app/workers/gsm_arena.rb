class GsmArena

    include Sidekiq::Worker

    def self.perform(url)
    	agent = Mechanize.new
    	specs = {}
        page = agent.get(url)
        name = page.at("div#ttl").text.strip
            specs[:name] = name
        if page.at("div#specs-list > p")
            p other_name = page.at("div#specs-list > p").text.strip
            specs[:alias_name] = other_name
        end
        specs[:img_url] = page.at("div#specs-cp-pic//a//img").attr("src")
        page.search("div#specs-list//table").each do |table_data|
            features = {}
            p grouping_parameters = table_data.at("th").text.strip
            table_data.search("tr").each do |table_text|
                p table_text.at("td.ttl").text.strip + "--" + table_text.at("td.nfo").text.strip if table_text.at("td").present?
                features[table_text.at("td.ttl").text.strip] = table_text.at("td.nfo").text.strip if table_text.at("td").present?
            end
            specs[grouping_parameters] = features
        end
        
        p specs
    end

end