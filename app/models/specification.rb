class Specification
  include Mongoid::Document
  field :feature, type: hash
  
  embedded_in :product
end
